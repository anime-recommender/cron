# pylint: disable=C0103,C0301

'''Cron Job Connection Setup'''

import os
import sys
import time
import ssl
import pika

hostname, port = '', -1
username, password, broker_id = '', '', ''

exchange_name, exchange_type, queue_name, routing_key = '', '', '', ''

SSL_ENABLED = False

# if os.environ.get('stage') == 'prod':
#     hostname = os.environ.get('rabbitmq_hostname')
#     port = os.environ.get('rabbitmq_port')

#     # Additional attributes required for production
#     username = os.environ.get('rabbitmq_username')
#     password = os.environ.get('rabbitmq_password')
#     region = os.environ.get('rabbitmq_region')
#     broker_id = os.environ.get('rabbitmq_broker_id=b-dab7cb2c-fe65-41c0-9bd1-2146728650bd')

#     exchange_name = os.environ.get('exchange_name')
#     exchange_type = os.environ.get('exchange_type')
#     queue_name = os.environ.get('queue_name')
#     routing_key = os.environ.get('routing_key')

#     SSL_ENABLED = True

if os.environ.get('stage') == 'dev' or os.environ.get('stage') == 'prod':
    hostname = os.environ.get('rabbitmq_hostname')
    port = os.environ.get('rabbitmq_port')

    exchange_name = os.environ.get('exchange_name')
    exchange_type = os.environ.get('exchange_type')
    queue_name = os.environ.get('queue_name')
    routing_key = os.environ.get('routing_key')

else:
    hostname = 'localhost'
    port = 5672
    exchange_name = "recommenderUpdate.topic"
    exchange_type = "topic"
    queue_name = 'recommenderEngine'
    routing_key = 'interactionDataPush.*'

parameters = None

if SSL_ENABLED:
    credentials = pika.PlainCredentials(username, password)
    context = ssl.SSLContext(ssl.PROTOCOL_TLSv1_2)

    parameters = pika.ConnectionParameters(
        host=hostname,
        port=port,
        virtual_host='/',
        credentials=credentials,
        ssl_options=pika.SSLOptions(context)
    )
else:
    # parameters = pika.ConnectionParameters(host=hostname, port=port)
    ###################################################################
    username = os.environ.get('rabbitmq_username')
    password = os.environ.get('rabbitmq_password')

    credentials = pika.PlainCredentials(username, password)
    parameters = pika.ConnectionParameters(
        host=hostname,
        port=port,
        # virtual_host='/',
        credentials=credentials,
        # ssl_options=pika.SSLOptions(context)
    )

connected = False
start_time = time.time()

print('Connecting...')

while not connected:
    try:
        connection = pika.BlockingConnection(parameters)
        connected = True
    except pika.exceptions.AMQPConnectionError:
        if time.time() - start_time > 20:
            sys.exit(1)

print('Connected!')

# Create an AMQP topic exchange for recommender updates
channel = connection.channel()

channel.exchange_declare(exchange=exchange_name,
                         exchange_type=exchange_type, durable=True)

# Create a queue for recommender engine that
# captures ALL updates from users
queue = channel.queue_declare(queue=queue_name, durable=True)
queue_length = queue.method.message_count
channel.queue_bind(exchange=exchange_name, queue=queue_name, routing_key=routing_key)

# Print the number of messages in the queue
# print('# messages in the queue : ' + str(queue_length))
