# pylint: disable=C0103,C0301,C0209,W0621

'''Cron Job'''

import os
import json
from urllib.parse import urlparse
import psycopg2

import amqp_setup

db_url, db_name = None, ''

if os.environ.get('stage'):
    db_url = urlparse(os.environ.get('db_conn'))
    db_name = os.environ.get('db_name')
else:
    db_url = urlparse("postgresql+psycopg2://postgres:example@localhost:5433")
    db_name = 'test_db'


def callback(record):
    '''Ingestion Job'''

    try:
        cnx = psycopg2.connect(
            user=db_url.username,
            password=db_url.password,
            host=db_url.hostname,
            port=db_url.port,
            database=db_name
        )
        cursor = cnx.cursor()

        user_id = record['user_id']
        anime_id = record['anime_id']
        category = record['category']
        score = record['score']
        date_time = record['date_time']

        # Ingest logs
        try:
            cursor.execute("INSERT INTO logs (user_id, anime_id, score, date_time) VALUES ("
                           + "'" + user_id + "'"
                           + ", '" + anime_id + "'"
                           + ", " + str(score)
                           + ", '" + date_time + "');")
            cnx.commit()
        except psycopg2.errors.UniqueViolation as e:
            return

        # Update utility_matrix
        cursor.execute('SELECT * FROM utility_matrix WHERE user_id = \'' + str(user_id) + '\';')
        record = cursor.fetchone()

        user_preferences = None

        if not record:

            user_preferences_str = '{"Romance": 0, "Sports": 0, "Mystery": 0, "Psychological": 0, "Music": 0, "Fantasy": 0, "Slice of Life": 0, "Mecha": 0, "Horror": 0, "Drama": 0, "Action": 0, "Supernatural": 0, "Thriller": 0, "Sci-Fi": 0, "Comedy": 0, "Ecchi": 0, "Adventure": 0}'

            cursor.execute("INSERT INTO utility_matrix (user_id, user_preferences) VALUES ("
                           + "'" + str(user_id) + "', '" + user_preferences_str + "');")
            cnx.commit()

            user_preferences = json.loads(user_preferences_str)
        else:

            user_preferences = json.loads(record[1])

        user_preferences[category] += score

        cursor.execute("UPDATE utility_matrix SET user_preferences = '" + json.dumps(user_preferences) + "' WHERE user_id = '" + str(user_id) + "'; ")

        cnx.commit()

        cnx.close()

        print(f"SUCCESS,{record}\n")

    except psycopg2.OperationalError as err:
        print(f"FAIL,{record},{err}\n")


connection = amqp_setup.connection
channel = amqp_setup.channel
queue_name = amqp_setup.queue_name
queue_length = amqp_setup.queue_length

if queue_length > 0:
    count = 0

    for method_frame, properties, body in channel.consume(queue_name):

        record = json.loads(body.decode('UTF-8'))

        print(body)

        callback(record)

        channel.basic_ack(method_frame.delivery_tag)

        count += 1

        if count >= queue_length:
            break

    # Cancel the consumer and return any pending messages
    requeued_messages = channel.cancel()
    print('Requeued %i messages' % requeued_messages)

    # Close the channel and the connection
    channel.close()
    connection.close()
